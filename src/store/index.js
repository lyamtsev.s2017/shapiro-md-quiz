import Vue from 'vue'
import Vuex from 'vuex'
import {constants} from "@/constants";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    answers: {},
    refer: null,
    offerText: null,
  },
  mutations: {
    SET_ANSWERS(state, answers) {
      state.answers = answers
    },
    SET_ANSWER(state, {answerKey, value, valueKey }) {
      valueKey = valueKey || 'default'
      if (!state.answers[answerKey]) {
        Vue.set(state.answers, answerKey, {})
      }
      Vue.set(state.answers, answerKey, {
        ...state.answers[answerKey],
        [valueKey]: value,
      })
    },
    SET_REFER(state, data) {
      state.refer = data
    },
    SET_OFFER_TEXT(state, text) {
      state.offerText = text
    }
  },
  actions: {},
  getters: {
    answers: state => state.answers,
    answer: state => answerKey => state.answers[answerKey],
    refer: state => state.refer,
    offerText: state => state.offerText
  },
  modules: {}
})
