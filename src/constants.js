
export const constants = {
  ANSWERS_KEYS: {
    // QUIZZES
    GET_STARTED: 'GET_STARTED',
    INTERSTITIAL: 'INTERSTITIAL',
    YOUR_HAIR: 'YOUR_HAIR',
    YOUR_HEALTH: 'YOUR_HEALTH',
    THE_BASICS: 'THE_BASICS',
    SHIPPING: 'SHIPPING',
    // MISC
    SMS_UPDATES: 'SMS_UPDATES',
    CROSS_SELL: 'CROSS_SELL',
  },
  // PRODUCTS
  SHAMPOO: 'Shampoo',
  CONDITIONER: 'Conditioner',
  ONE_ADDITIONAL_VISIT: '1x Additional Doctor Visit',
  TWO_ADDITIONAL_VISITS: '2x Additional Doctor Visit',
  // END PRODUCTS
  NONE_CHOICE: 'None of the above',
  MINOXIDIL: 'Minoxidil (Rogaine)',
  FINASTERIDE: 'Finasteride (Propecia)',
  OTHER_TREATMENTS: 'Other Treatments',
  MONEY_BACK_GUARANTEE: '90-DAY MONEY BACK GUARANTEE'
}
